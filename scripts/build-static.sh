h5pDir="./public/h5p"
mkdir -p ${h5pDir}

echo "copy h5p-core"
mkdir -p ${h5pDir}/core
cp -r ./h5p/core ${h5pDir}

echo "copy h5p librairies and content"
cp -r ./h5p-server/* ${h5pDir}

echo "copy h5p-contents and restructure them"
# see https://github.com/Lumieducation/H5P-Nodejs-library/issues/532
# for dir in h5p-server/content/* ; do 
#     echo $dir;
#     contentId=$(basename $dir)

#     echo $contentId
#     mkdir -p ./build/content/${contentId} #/content
#     cp -r $dir/* ./build/content/${contentId} #/content
#     #mv ./build/content/${contentId}/content/h5p.json ./build/content/${contentId}
#     # add math -lib to libraries
#     # node ./scripts/add-math-lib.js ./build/${contentId}/h5p.json
# done


