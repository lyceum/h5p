const H5P = require("h5p-nodejs-library");
const fs = require("fs");
const path = require("path");

const renderer = require("./create-pages-renderer");

// TODO why this do never exit?
const start = async () => {
  console.log("Build html pages to embed dir");
  const contentManager = new H5P.fsImplementations.FileContentStorage(
    path.resolve("h5p-server/content")
  );
  const libraryManager = new H5P.fsImplementations.FileLibraryStorage(
    path.resolve("h5p-server/libraries")
  );

  // player
  const player = new H5P.H5PPlayer(
    libraryManager,
    contentManager,
    new H5P.H5PConfig(undefined, {
      baseUrl: "/h5p",
    })
  );
  player.renderer = renderer;
  // get h5p ids as list
  const contentIds = await contentManager.listContent();

  contentIds.forEach((contentId) => {
    console.log("Writing " + contentId + "/embed");

    player.render(contentId).then((html) => {
      const embedDir = `./public/${contentId}/embed`;
      if (!fs.existsSync(embedDir)) {
        fs.mkdirSync(embedDir, { recursive: true });
      }
      fs.writeFileSync(`${embedDir}/index.html`, html);
    });
  });
  return "Done";
};

start().then(console.log).catch(console.error);
