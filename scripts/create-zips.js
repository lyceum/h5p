const H5P = require("h5p-nodejs-library");
const fs = require("fs");
const path = require("path");

// TODO why this do never exit?
const start = async () => {
  console.log("Build h5p files...");
  const contentManager = new H5P.fsImplementations.FileContentStorage(
    path.resolve("h5p-server/content")
  );
  const libraryManager = new H5P.fsImplementations.FileLibraryStorage(
    path.resolve("h5p-server/libraries")
  );

  // zip exporter
  const packageExporter = new H5P.PackageExporter(
    libraryManager,
    contentManager,
    255
  );

  // get h5p ids as list
  const contentIds = await contentManager.listContent();

  contentIds.forEach((contentId) => {
    console.log("Writing " + contentId + ".h5p");
    const exericeDir = `./public/${contentId}`;
    if (!fs.existsSync(exericeDir)) {
      fs.mkdirSync(exericeDir, { recursive: true });
    }
    const writeStream = fs.createWriteStream(`${exericeDir}/${contentId}.h5p`);
    packageExporter.createPackage(contentId, writeStream);
  });
  return "Done";
};

start().then(console.log).catch(console.error);
