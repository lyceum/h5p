// Adapted from https://github.com/Lumieducation/H5P-Nodejs-library/blob/master/src/renderers/player.ts
module.exports = (model) => {
  // model.integration.url = "/embed"; //+ model.contentId;
  model.scripts.push(
    "/h5p/libraries/H5P.MathDisplay-1.0/scripts/mathdisplay.js"
  );
  return `<!doctype html>
<html class="h5p-iframe">
<head>
    <meta charset="utf-8">
    
    ${model.styles
      .map((style) => `<link rel="stylesheet" href="${style}"/>`)
      .join("\n    ")}
    ${model.scripts
      .map((script) => `<script src="${script}"></script>`)
      .join("\n    ")}
    <script>
        H5PIntegration = ${JSON.stringify(model.integration, null, 2)};
    </script>
</head>
<body>
    <div class="h5p-content" data-content-id="${model.contentId}"></div>
    <script>
    /*
     * ajuster la taille de l'iframe par POST MESSAGE
     * https://stackoverflow.com/a/6940531
    */ 
    const originUrl = "https://www.lyceum.fr"
    // const originUrl =  "http://localhost:8000"

    function sendResizeMessage() {
      const body = document.querySelector('body');
      const html = document.querySelector('html');
      const height = Math.max(
        body.scrollHeight,
        body.offsetHeight,
        html.clientHeight,
        html.scrollHeight,
        html.offsetHeight
      );
      window.parent.postMessage({
        eventId: "iframe-height-data", 
        iframeId: ${model.contentId}, 
        height: height
      }, 
        originUrl);

    }
    window.addEventListener('message', function(event) {
      if (event.origin !== originUrl) return;
      const { eventId, iframeId } = event.data
      if (eventId !== 'iframe-need-resize') return
      if (parseInt(iframeId) !== ${model.contentId}) return
      sendResizeMessage()
    }, false);
    
    window.onload = function() {
      sendResizeMessage()
    }
    </script>
</body>
</html>`;
};
