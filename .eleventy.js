module.exports = (eleventyConfig) => {
  // Copy our static assets to the output folder
  eleventyConfig.addPassthroughCopy({
    "./eleventy/favicons/": "/",
    "./eleventy/js/": "/",
    "./eleventy/css/": "/css",
  });
  return {
    dir: {
      input: "eleventy",
      output: "public",
    },
  };
};
