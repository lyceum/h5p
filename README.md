# Exercices interactifs h5p pour lyceum.fr

Les exercices sont accessibles à l'adresse: http://h5p.lyceum.fr.

## Développement

Les exercices sont crées localement grâce à la librairie
[`H5P-Nodejs-library`](https://github.com/Lumieducation/H5P-Nodejs-library).

L'application est dans le répertoire `src`. Pour la lancer:

```sh
yarn
yarn start
```

The content and libraries are stored on the `h5p-server` folder which is then be turned
in a static site on the `public` directory using:

- `H5P-Nodejs-library player` to create static pages of the exercices
- and [`elevently`](https://www.11ty.dev) to generate the static website.
