const fs = require("fs");
const fsPromise = require("fs").promises;
const path = require("path");

// let h5ps = JSON.parse(fs.readFileSync("eleventy/_data/h5ps.json", "utf8"));
// console.log(h5ps);
// A faire sur tous

module.exports = async function () {
  let h5ps = [];
  const rootDir = path.dirname(path.dirname(__dirname));
  const contentDir = path.join(rootDir, "h5p-server/content/");
  const ids = await fsPromise.readdir(contentDir);

  ids.forEach((id) => {
    let h5p = JSON.parse(
      fs.readFileSync(path.join(contentDir, id, `h5p.json`), "utf8")
    );
    h5p.id = id;
    h5ps.push(h5p);
  });

  console.log("Writing h5ps.json");
  fs.writeFileSync(
    path.join(rootDir, "public", "h5ps.json"),
    JSON.stringify(h5ps)
  );

  return Promise.resolve(h5ps);
};
