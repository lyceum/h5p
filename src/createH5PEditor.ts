import * as H5P from "h5p-nodejs-library";

/**
 * Create a H5PEditor object.
 * using the local
 * filesystem storage classes will be used.
 *
 * CONTENTSTORAGE=mongos3 Uses MongoDB/S3 backend for content storage
 * CONTENT_MONGO_COLLECTION Specifies the collection name for content storage
 * CONTENT_AWS_S3_BUCKET Specifies the bucket name for content storage
 * TEMPORARYSTOAGE=s3 Uses S3 backend for temporary file storage
 * TEMPORARY_AWS_S3_BUCKET Specifies the bucket name for temporary file storage
 *
 * Further environment variables to set up MongoDB and S3 can be found in
 * docs/mongo-s3-content-storage.md and docs/s3-temporary-file-storage.md!
 * @param config the configuration object
 * @param localLibraryPath a path in the local filesystem in which the H5P libraries (content types) are stored
 * @param localContentPath a path in the local filesystem in which H5P content will be stored (only necessary if you want to use the local filesystem content storage class)
 * @param localTemporaryPath a path in the local filesystem in which temporary files will be stored (only necessary if you want to use the local filesystem temporary file storage class).
 * @returns a H5PEditor object
 */
export default async function createH5PEditor(
  config: H5P.IH5PConfig,
  localLibraryPath: string,
  localContentPath?: string,
  localTemporaryPath?: string
): Promise<H5P.H5PEditor> {
  // Depending on the environment variables we use different implementations
  // of the storage interfaces.
  const h5pEditor = new H5P.H5PEditor(
    new H5P.fsImplementations.InMemoryStorage(), // this is a general-purpose cache
    config,
    new H5P.fsImplementations.FileLibraryStorage(localLibraryPath),
    new H5P.fsImplementations.FileContentStorage(localContentPath),
    new H5P.fsImplementations.DirectoryTemporaryFileStorage(localTemporaryPath)
  );

  return h5pEditor;
}
